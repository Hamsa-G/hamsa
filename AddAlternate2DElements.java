public interface AddAlternate2DElements {
	void get2DArrayElements();

	void addAlternateElements();

	void printSumOfAlternateElements();
}