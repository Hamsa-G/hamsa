#include<stdio.h>
#include<stdlib.h>
#define MAX_TEST 1000
#define MIN_TEST 1
#define MAX_FRIEND 100000
#define MIN_FRIEND 1
#define MAX_DEL_FRIEND 100000
#define MIN_DEL_FRIEND 0

struct node{
    int data;
    struct node *next;
};

int validate_constraints(int max,int min,int input){
    if(input>=min && input<=max)
    return 1;
    else return 0;
}

struct node* createNode(int data)
{
    struct node* newNode = (struct node*) malloc(sizeof(struct node));
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}
 
struct node* push(struct node* top, struct node* newNode){
    if(top)
        newNode->next = top;
    return newNode;
}
 
struct node* pop(struct node *top){
    if(top == NULL)
        return top;
    struct node *temp = top->next;
    free(top);
    return temp;
}
 
void printList(struct node* head){
    if(head == NULL)
        return;
    printList(head->next);
    printf("%d ", head->data);
}

void delete_friend(int total_friends,int friends_to_delete){
    int del,n,counter,temp;
    struct node *top = NULL, *newNode;
    
    if(validate_constraints(MAX_FRIEND,MIN_FRIEND,total_friends) && 
    validate_constraints(MAX_DEL_FRIEND,MIN_DEL_FRIEND,friends_to_delete)){
    
    del = friends_to_delete;
    n = total_friends;
        
    for(counter=0; counter<n; counter++) {
        scanf("%d", &temp);
        newNode = createNode(temp);
        while(top!=NULL && top->data < temp && del){
                del--;
                top = pop(top);
            }
          top = push(top, newNode);
        }
        printList(top);
        printf("\n");
    } 
}

int main(){
    int test_cases,total_friends,friends_to_delete,loop;
    scanf("%d", &test_cases);
    
    if(validate_constraints(MAX_TEST,MIN_TEST,test_cases)) {
    for(loop=0; loop<test_cases; loop++) {
        scanf("%d %d", &total_friends, &friends_to_delete);
        delete_friend(total_friends,friends_to_delete);
       }
    }    
    return 0;
}