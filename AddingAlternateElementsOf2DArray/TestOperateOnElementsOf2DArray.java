import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.Assert;
import org.junit.Test;

public class TestOperateOnElementsOf2DArray {
	@Test
	public void testInputElementsInArray() {

		// Arrange
		OperateOnElementsOfArray operate = new OperateOnElementsOfArray();

		// Act
		operate.get2DArrayElements();

		// Assert
		int inputArray[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		assertArrayEquals(inputArray, operate.array);
	}

	@Test
	public void testAdditionOfAlternateElementsOfArray() {
		// Arrange
		OperateOnElementsOfArray operate = new OperateOnElementsOfArray();
		operate.get2DArrayElements();

		// Act
		operate.addAlternateElements();

		// Assert
		assertEquals(25, operate.evenSum);
		assertEquals(20, operate.oddSum);

	}
}