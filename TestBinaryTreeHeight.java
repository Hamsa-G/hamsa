import static org.junit.Assert.*;
import org.juint.Test;

public class TestBinaryTreeHeight {

@Test
public void insertNewnode_ReturnsNewnode {
    MonkBreakingArray tester = new MonkBreakingArray();
    Node root = null;
    assertEquals(5, tester.insertNewnode(root,data).value);
}

@Test
public void positionOfNewnode_insertNodeAsRoot {
    MonkBreakingArray tester = new MonkBreakingArray();
    Node root = null;
    root = tester.insertNewnode(root,5);
    assertNotNull(tester.root);
}

@Test
public void positionOfNewnode_insertNodeAsLeftChild {
    MonkBreakingArray tester = new MonkBreakingArray();
    Node root = null;
    root = tester.insertNewnode(root,5);
    tester.insertNewNode(root,4); 
    assertNotNull(tester.root);
    assertEquals(4,root.left.value);
}

@Test
public void positionOfNewnode_insertNodeAsRightChild {
    MonkBreakingArray tester = new MonkBreakingArray();
    Node root = null;
    root = tester.insertNewnode(root,5);
    tester.insertNewNode(root.left,6); 
    assertNotNull(tester.root);
    assertEquals(6,child.right.value);
}

@Test
public void findHeightOfTree_getHeight {
    MonkBreakingArray tester = new MonkBreakingArray();
    Node root = null;
    tester.insertNewnode(root,1);
    tester.insertNewnode(root,2);
    tester.insertNewnode(root,3);
    tester.insertNewnode(root,4);
    int expectedHeight = 4;
    int actualHeight = tester.findHeightOfBinaryTree(root);
    assertEquals(expectedHeight,actualHeight);
}

