//
//  ViewController.swift
//  FacebookFeed
//
//  Created by Shrirang Andurkar on 4/6/18.
//  Copyright © 2018 In Time Tec. All rights reserved.
//

import UIKit
import CoreGraphics

class SignUpViewController: UIViewController, UITextFieldDelegate, GetStatusParserDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var userNameTxt: UITextField!
    @IBOutlet var departmentTxt: UITextField!
    @IBOutlet var designationTxt: UITextField!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var confirmPwdTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var contactTxt: UITextField!
    @IBOutlet var signUpBtn: UIButton!
    private var parser : SignUpParser?
    var picker: UIPickerView?
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    let departmentList = ["HR", "Operational","IT","Dev"]
    let designationList = ["Jr.Software Engineer", "software Engineer", "Technical lead", "Project manager", "UI Architect", "HR generalist", "CEO", "CTO", "System Admin"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        parser = SignUpParser()
        parser?.delegate = self
        activityIndicator.isHidden = true
        picker = UIPickerView(frame: CGRect(x: 0, y: 200, width: view.frame.width, height: 220))
        picker?.delegate = self
        picker?.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(updatePicker), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        createPickerView()
    }
    @objc func updatePicker(){
        self.picker?.reloadAllComponents()
    }
    func createPickerView(){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        
        departmentTxt.inputAccessoryView = toolbar
        designationTxt.inputAccessoryView = toolbar
        
        departmentTxt.inputView = picker
        designationTxt.inputView = picker
    }
    
    @objc func donePressed(){
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0...3:
            print("do nothing")
        default:
             scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    func validateUI() -> Bool{
        if (userNameTxt.text!).isEmpty || (departmentTxt.text!).isEmpty || (designationTxt.text!).isEmpty || (emailTxt.text!).isEmpty || (passwordTxt.text!).isEmpty || (confirmPwdTxt.text!).isEmpty || (contactTxt.text!).isEmpty {
            return false
        }
        return true
    }
    
    func validateUserName(str: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[0-9a-zA-Z\\_ ]{7,18}$", options: .caseInsensitive)
            if regex.matches(in: str, options: [], range: NSMakeRange(0, str.count)).count > 0 { return true}
        } catch {
            print(error.localizedDescription)
        }
        return false
    }
    
    func validateEmail(str: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: str, options: [], range: NSRange(location: 0, length: str.count)) != nil
    }
    
    func validateDepartment(str: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z. ]{2,18}$", options: .caseInsensitive)
            if regex.matches(in: str, options: [], range: NSMakeRange(0, str.count)).count > 0 { return true}
        } catch {
            print(error.localizedDescription)
        }
        return false
    }

    func validateDesignation(str: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z. ]{2,18}$", options: .caseInsensitive)
            if regex.matches(in: str, options: [], range: NSMakeRange(0, str.count)).count > 0 { return true}
        } catch {
            print(error.localizedDescription)
        }
        return false
    }
    
    func validateContact(str: String) -> Bool{
        let PHONE_REGEX = "^\\d{10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: str)
        return result
    }
    func validatePwd(str: String) -> Bool {
        print("lenght is \(str.count)")
        if str.count == 8 {
            return true
        }
        else {
            return false
        }
    }
    func didReceiveStatusValue(_ signUpStatus: SignUpModel) {
        UserDefaults.standard.set(signUpStatus.httpCode, forKey: "httpCode")
        if signUpStatus.httpCode == 201 {
            navigateToOTPScreen()
        }
    }
    
    private func navigateToOTPScreen(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "otpVerificationStoryBoard") as? OtpVerificationViewController
        
        nextViewController?.email = emailTxt.text
        self.present(nextViewController!, animated:true, completion:nil)
    }
    
    func didReceiveError(errorMessage:String) {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        showAlert(title: "Error", message: errorMessage)
        enableUI()
    }
    func enableUI(){
        userNameTxt.isEnabled = true
        departmentTxt.isEnabled = true
        designationTxt.isEnabled = true
        contactTxt.isEnabled = true
        emailTxt.isEnabled = true
        passwordTxt.isEnabled = true
        confirmPwdTxt.isEnabled = true
        signUpBtn.isEnabled = true
    }

    func disableUI(){
        userNameTxt.isEnabled = false
        departmentTxt.isEnabled = false
        designationTxt.isEnabled = false
        contactTxt.isEnabled = false
        emailTxt.isEnabled = false
        passwordTxt.isEnabled = false
        confirmPwdTxt.isEnabled = false
        signUpBtn.isEnabled = false
    }
    
    @IBAction func handleSignup(_ sender: UIButton) {
        disableUI()
        if validateUI() && validateUserName(str: userNameTxt.text!) && validateDepartment(str: designationTxt.text!) && validateEmail(str: emailTxt.text!) && validateDesignation(str: designationTxt.text!) && validateContact(str: contactTxt.text!) && validatePwd(str: passwordTxt.text!) && passwordTxt.text! == confirmPwdTxt.text! {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            let dictionary = [
                "password": self.passwordTxt.text,
                "screenName": self.userNameTxt.text,
                "emailAddress": self.emailTxt.text,
                "mobileNumber": self.contactTxt.text,
                "designation": self.designationTxt.text,
                "department": self.departmentTxt.text
            ]
            parser?.signUpUser(dictionary: dictionary)
        }
        else {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
            showAlert(title: "Error", message: "Fill the form with valid values")
            enableUI()
        }
    }
    func cleanCode() {
        userNameTxt.text = ""
        designationTxt.text = ""
        departmentTxt.text = ""
        contactTxt.text = ""
        emailTxt.text = ""
        passwordTxt.text = ""
        confirmPwdTxt.text = ""
    }
    func showAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

