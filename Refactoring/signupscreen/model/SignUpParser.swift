//
//  SignUpParser.swift
//  bookmyassets
//
//  Created by Intime on 04/06/18.
//  Copyright © 2018 In Time Tec. All rights reserved.
//

import UIKit

@objc protocol GetStatusParserDelegate : NSObjectProtocol {
    func didReceiveStatusValue(_ signUpStatus : SignUpModel)
    @objc optional func didReceiveError(errorMessage:String)
}

class SignUpParser: NSObject {
    weak var delegate : GetStatusParserDelegate?
    
    
    func signUpUser(dictionary: [String:Any]){
        let url = URL(string: "http://192.168.6.124:8080/GenericBookingSystem/createUser/1")
        var request = URLRequest(url:url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            
            guard error == nil else {
                self.updateUI(statusCode: 0, response: [:])
                return
            }
            
            guard let data = data else {
                self.updateUI(statusCode: 0, response: [:])
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 409 {
                print("statusCode should be 201, but is \(httpStatus.statusCode)")
                self.updateUI(statusCode: httpStatus.statusCode, response: [:])
                return
            }
            
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let httpStatus = json["httpCode"] as? Int
                    self.updateUI(statusCode: httpStatus!, response: json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
            
            
        })
        task.resume()
    }
    
    private func updateUI(statusCode:Int,response:[String:Any]){
        switch statusCode {
        case 409:
            DispatchQueue.main.async {
                if self.delegate != nil {
                    if self.delegate!.responds(to: #selector(GetStatusParserDelegate.didReceiveError)){
                        self.delegate!.didReceiveError!(errorMessage: "User Email already exists")
                    }
                }
            }
        case 201:
            let status = response["httpCode"] as? Int
            let signUpModel = SignUpModel(code: status!)
            DispatchQueue.main.async {
                if self.delegate != nil {
                    self.delegate?
                        .didReceiveStatusValue(signUpModel)
                }
            }
            
        default:
            DispatchQueue.main.async {
                if self.delegate != nil {
                    if self.delegate!.responds(to: #selector(GetStatusParserDelegate.didReceiveError)){
                        self.delegate!.didReceiveError!(errorMessage: "An error occured. Please try again later")
                    }
                }
            }
        }
    }
}
