//
//  Extension.swift
//  bookmyassets
//
//  Created by Intime on 06/06/18.
//  Copyright © 2018 In Time Tec. All rights reserved.
//

import Foundation
import UIKit
extension SignUpViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if departmentTxt.isFirstResponder {
            return departmentList.count
        } else {
            return designationList.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if departmentTxt.isFirstResponder {
            return departmentList[row]
        } else {
            return designationList[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if departmentTxt.isFirstResponder {
            var itemSelected = departmentList[row]
            departmentTxt.text = itemSelected
        } else {
            var itemSelected = designationList[row]
            designationTxt.text = itemSelected
        }
    }
    
}
