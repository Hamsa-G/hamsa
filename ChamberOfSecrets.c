#include <stdio.h>
#define MIN_SELECT 1
#define MAX_SELECT 316

struct spider
{
   int power;
   int position;
};

int validate_constraints(int,int,int);
int find_max_power(struct spider a[],int);

int main()
{
    int loop,counter,arrind=0,no_of_spiders,selected_spiders,min_spider,max_spider,n,x;
    scanf("%d %d",&no_of_spiders,&selected_spiders);
    
    n=no_of_spiders;
    x=selected_spiders;
    
    struct spider org[n];
    struct spider temp[x];
    min_spider=x;
    max_spider=x*x;
    
    if(validate_constraints(MIN_SELECT,MAX_SELECT,x) && validate_constraints(min_spider,max_spider,n))
    {
      for(int i=0;i<n;i++)
      {  
	scanf("%d",&org[i].power); 
	org[i].position = i+1;
      }
    }
     for(loop=0;loop<x;loop++)
	{
	    if(n>x)
	     { 
		    for(counter=0;counter<x;counter++)
		      {
    		       temp[counter] = org[counter];
           	     } 
	          arrind=find_max_power(temp,x); 
		    for(loop=arrind;loop<x-1;loop++)
		     { 
			    temp[loop] = temp[loop+1];             
		     }         
		    for(counter=0;counter< (n-x) ; counter++)
		     {  
			    org[counter] = org[x+counter];         
		     }
		    for (loop=n-x,counter=0;loop<(n-1);loop++,counter++)
		     { 
 			    org[loop] = temp[counter];          
			    if(org[loop].power > 0)        
			    org[loop].power--;
 		     } 
		  n--;
	     }  
	   else
	    {
 		   arrind=find_max_power(org,n);
		   for(loop=arrind;loop<n;loop++)
 		    {  
			   org[loop]=org[loop+1];              
		    }
		   n--;  
		   for(counter=0;counter<n;counter++)  
		    {   
			  if(org[counter].power > 0)      
			  org[counter].power--;
		    }
	    }
    }
   return 0;
}
    
int validate_constraints(int low_limit,int up_limit,int inp) 
{   
   if(inp>=low_limit && inp<=up_limit)   
   return 1;
   else return 0;
}
    
int find_max_power(struct spider a[],int x)  
{  
	int max=-1,max_ind=0,index,i;  
	for(i=0;i<x;i++)  
	{     
		if(a[i].power>max)   
		{
 			max=a[i].power; 
			max_ind=a[i].position;
			index=i;
 		}
	}  
	printf("%d ",max_ind);  
	return index;
}