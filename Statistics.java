import java.util.*;

class Statistics {
        static Scanner scan = new Scanner (System.in);
        static HashMap<String,Integer>listOfSports = new HashMap<>();
        static HashMap<String,String>listOfEntries = new HashMap<>();
        
public static void insertSportsAndNameIntoList(){
        int noOfEntries = scan.nextInt();
        for (int entries=0; entries < noOfEntries; entries++){
            String name = scan.next();
            String sport = scan.next();
            
            if(!listOfEntries.containsKey(name))
            listOfEntries.put(name,sport);
        }
}
public static void evaluateSportList(){
        for( String sport:listOfEntries.values())
            {
              if(!listOfSports.containsKey(sport)) 
              listOfSports.put(sport,1);
              else listOfSports.put(sport,listOfSports.get(sport)+1);
            }
}
public static String findFavSport(){
        String resultingFavSport="";
        int maxFans=Integer.MIN_VALUE;
		Set<String> sportNames = listOfSports.keySet();
		for (String sportname : sportNames) {
 		    int likers = listOfSports.get(sportname);
 			    if (likers > maxFans) {
					maxFans = likers;
					resultingFavSport = sportname;
				}
 			}
		return resultingFavSport;
        
}
public static void printFavSportAndFootballLovers(String FavSportName){
    System.out.println(FavSportName);
    System.out.println(listOfSports.get("football")==null?"0":listOfSports.get("football"));
}

public static void main(String args[]){
        insertSportsAndNameIntoList();
        evaluateSportList();
        String FavSportName = findFavSport();
        printFavSportAndFootballLovers(FavSportName);
}
                
}