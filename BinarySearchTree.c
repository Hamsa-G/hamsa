#include<stdio.h>

struct node{
    int data;
    struct node *left;
    struct node *right;
};

struct node* insert_newnode_into_tree(struct node* root,int data){
    if(root == NULL){
        struct node* first_element = (struct node*)malloc(sizeof(struct node*));
        first_element->data = data;
        first_element->left = NULL;
        first_element->right = NULL;
        return first_element;
    }
    else if(data <= root->data)
        root->left = insert_newnode_into_tree(root->left,data);
    else root->right = insert_newnode_into_tree(root->right,data);
    
    return root;
}

int find_height_of_binaryTree(struct node* node){
    if (node==NULL) 
       return 0;
    else
      {
       int left_tree_depth = find_height_of_binaryTree(node->left);
       int right_tree_depth = find_height_of_binaryTree(node->right);
 
       if (left_tree_depth > right_tree_depth) 
           return(left_tree_depth+1);
       else return(right_tree_depth+1);
   }
}

int main()
{
	struct node *root = NULL;
	int no_of_arrayElements, index, element, height_of_binaryTree;
	scanf("%d",&no_of_arrayElements);
	for (index = 0; index < no_of_arrayElements; index++)
	{
		scanf("%d",&element);
		root = insert_newnode_into_tree(root, element);
	}
	height_of_binaryTree = find_height_of_binaryTree(root);
	printf("%d",height_of_binaryTree);
	return 0;
}